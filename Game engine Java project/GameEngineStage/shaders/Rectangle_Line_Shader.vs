#version 450 core

layout(location=0) in vec3 vertices;
layout(location=1) in vec4 in_color;
layout(location=0) out vec4 pass_color;

uniform mat4 orthoProjectionMatrix;
uniform mat4 transformationMatrixObject;
uniform mat4 transformationMatrixCamera;
void main(){
	gl_Position=orthoProjectionMatrix*(transformationMatrixCamera*(transformationMatrixObject*vec4(vertices,1.0f)));
	pass_color=in_color;
}