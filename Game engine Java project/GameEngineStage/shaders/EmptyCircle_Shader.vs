#version 450 core

layout(location=0) in vec3 vertices;
uniform mat4 orthoProjectionMatrix;
uniform mat4 transformationMatrixObject;
uniform mat4 transformationMatrixCamera;

void main(){
	gl_Position=orthoProjectionMatrix*(
	                transformationMatrixCamera*(
	                    transformationMatrixObject*vec4(vertices,1.0f)
	                )
	            );
}