#version 450 core

layout(location=0) in vec3 vertices;
uniform mat4 orthoProjectionMatrix;
uniform mat4 transformationMatrixObject;
uniform mat4 transformationMatrixCamera;
uniform vec4 internalColor;
uniform vec4 externalColor;
layout(location=0) out vec4 pass_color;
void main(){
	gl_Position=orthoProjectionMatrix*(transformationMatrixCamera*(transformationMatrixObject*vec4(vertices,1.0f)));
	if (gl_VertexID==0)
		pass_color=internalColor;
	else
		pass_color=externalColor;
}