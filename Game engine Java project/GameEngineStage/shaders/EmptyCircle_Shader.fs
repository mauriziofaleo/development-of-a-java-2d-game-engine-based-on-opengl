#version 450 core


uniform vec4 color;
uniform vec4 colorToAdd;
layout(location=0) out vec4 out_Color;
void main(){
	out_Color = color+colorToAdd;
}