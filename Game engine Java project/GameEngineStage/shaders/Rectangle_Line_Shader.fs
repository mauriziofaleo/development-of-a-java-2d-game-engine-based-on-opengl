#version 450 core

layout(location=0) in vec4 pass_color;
layout(location=0) out vec4 out_Color;
uniform vec4 colorToAdd;
void main(){
	out_Color = pass_color+colorToAdd;
}