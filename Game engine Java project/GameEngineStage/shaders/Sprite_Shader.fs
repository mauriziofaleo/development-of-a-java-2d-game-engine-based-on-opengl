#version 450 core

layout(location=0) in vec2 pass_textureCoords;
layout(location=0) out vec4 out_Color;
layout(binding=0) uniform sampler2D sampler;
uniform vec4 colorToAdd;
void main(){
	out_Color = texture(sampler,pass_textureCoords)+colorToAdd;
	if (out_Color.a<0.1) discard;
}