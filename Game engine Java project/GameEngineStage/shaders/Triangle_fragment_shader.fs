#version 450 core

layout(location=0) in vec4 received_color;
layout(location=0) out vec4 out_Color;

void main(){
	out_Color = received_color;
}