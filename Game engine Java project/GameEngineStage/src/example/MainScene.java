package example;
import java.io.IOException;
import Framework.Camera;
import Framework.Color;
import Framework.Scene;
import Framework.Theatre;
import example.bonus.BonusGenerator;
import example.enemies.EnemyGenerator;
import example.player.LaserQuantitaCornice;
import example.player.LaserQuantitaRettangolo;
import example.player.Player;

public class MainScene extends Scene {
	public Theatre theatre;
	
	public MainScene(Theatre theatre) throws IOException {
		this.theatre=theatre;
		BonusGenerator.scene=this;
		camera=new Camera(640, 360);
		setCamera(camera);
		camera.setX(320);
		camera.setY(180);
		setBackgroundColor(Color.BLACK);
		for (int n=0; n<40;n++) 
			addActorToTheScene(new Stella());
		Player player=new Player(this);
		addActorToTheScene(player);
		for (int n=0; n<9;n++) {
			Parete parete=new Parete(this);
			parete.setX(80+n*60);
			parete.setY(8);
			addActorToTheScene(parete);
		}
		addActorToTheScene(new EnemyGenerator(this));
		LaserQuantitaRettangolo laserQuantitaRettangolo=new LaserQuantitaRettangolo(player);
		addActorToTheScene(laserQuantitaRettangolo);
		LaserQuantitaCornice laserQuantitaCornice=new LaserQuantitaCornice(player);
		addActorToTheScene(laserQuantitaCornice);
	}
}
