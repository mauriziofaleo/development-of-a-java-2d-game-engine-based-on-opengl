package example;
import java.io.IOException;

import Framework.MemoryManager;
import Framework.Theatre;
import Framework.input.Input;
import Framework.input.KeyListener;

public class LaunchMe {
	public static void main(String[] args) throws IOException {
		Theatre theatre=new Theatre(1280,720,"Shoot game");
		theatre.setScene(new MainScene(theatre));
		
		Input.addKeyListener(new KeyListener() {
			@Override
			public void notifyEvent(int key, int actionType) {
				if (key==Input.KEY_ESCAPE) {
				    MemoryManager.freeMemory();
					System.exit(0);
				}
				else if(key==Input.KEY_P && actionType==Input.PRESS) {
					if (theatre.getSpeed()==0)
						theatre.setSpeed(1);
					else
						theatre.setSpeed(0);
				}
			}
		});
		Input.hideMouseCursor();
		theatre.act();
	}
}
