package example;

import java.io.IOException;
import java.util.Random;

import Framework.rendering.actors.SpriteActor;

public class Stella extends SpriteActor{
	Random random=new Random();
	float speed;
	public Stella() throws IOException {
		super("stella.png");
		setX(random.nextInt(640));
		setY(random.nextInt(360));
		setDistance(9);
		speed=30+random.nextInt(100);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void act(float deltaTime) {
		setY(getY()-deltaTime*speed);
		if (getY()<-100) {
			setY(370);
			setX(random.nextInt(640));
			speed=30+random.nextInt(100);
		}
		// TODO Auto-generated method stub
		
	}

}
