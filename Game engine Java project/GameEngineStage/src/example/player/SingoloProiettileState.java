package example.player;

import java.io.IOException;

import Framework.input.Input;

public class SingoloProiettileState extends PlayerState{

	public SingoloProiettileState(Player player) {
		super(player);
	}
	@Override
	public void act(float deltaTime) {
		
	}
	@Override
	public void notifyKeyPressed(int key, int actionType) {
		if (key==Input.MOUSE_BUTTON_LEFT && actionType==Input.PRESS) {
			try {
				Proiettile proiettile=new Proiettile(player.getX(),player.getY()+20,player.scene);
				player.scene.addActorToTheScene(proiettile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
