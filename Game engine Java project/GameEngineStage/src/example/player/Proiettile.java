package example.player;

import java.io.IOException;

import Framework.Scene;
import Framework.colliders.CircleCollider;
import Framework.rendering.actors.SpriteActor;
import Framework.rendering.interfaces.Actor;
import example.bonus.Bonus;

public class Proiettile extends SpriteActor {
	float speed=600;
	Scene scene;
	public Proiettile(float x,float y,Scene scene) throws IOException {
		super("proiettile.png");
		setX(x);
		setY(y);
		setDistance(2);
		addCollider(new CircleCollider(5, 0, 0));
		this.scene=scene;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void act(float deltaTime) {
		setY(getY()+speed*deltaTime);
		if (getY()>370) {
			scene.removeActorFromTheScene(this);
		}
		
	}

	@Override
	public void notifyBeginCollision(Actor o) {
		if (!(o instanceof Bonus || o instanceof Player)) {
			scene.removeActorFromTheScene(this);
		}
	}

}
