package example.player;

public abstract class PlayerState {
	Player player;
	public PlayerState(Player player) {
		this.player=player;
	}
	public abstract void act(float deltaTime);
	public abstract void notifyKeyPressed(int key,int actionType);
}
