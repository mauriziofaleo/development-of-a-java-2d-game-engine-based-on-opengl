package example.player;

import java.io.IOException;
import Framework.Color;
import Framework.Scene;
import Framework.colliders.CircleCollider;
import Framework.input.Input;
import Framework.input.KeyListener;
import Framework.input.MouseChangePositionListener;
import Framework.rendering.actors.EmptyCircleActor;
import Framework.rendering.actors.SpriteActor;
import Framework.rendering.interfaces.Actor;
import example.MainScene;
import example.bonus.BonusVita;
import example.enemies.Asteroide;
import example.enemies.Meteora;
import example.enemies.NemicoAereo;

public class Player extends SpriteActor {
	PlayerState currentState=new SingoloProiettileState(this);
	Scene scene;
	SpriteActor[] barreVita=new SpriteActor[12];
	int salute=12;
	double lastMouseX=0;
	double lastMouseY=0;
	public Player(Scene scene) throws IOException {
		super("player.png");
		Input.setMousePosition(0, 0);
		addCollider(new CircleCollider(15, 0, 0));
		this.scene=scene;
		setX(320);
		setY(40);
		setDistance(5);
		setRotation(180);
		for (int n=0;n<12;n++) {
			BarraVita b;
			if (n>=0 && n<4)
				b=new BarraVita("life_red.png");
			else if (n>=4 && n<8)
				b=new BarraVita("life_orange.png");
			else
				b=new BarraVita("life_green.png");
			b.setRotation(90);
			b.setX(10+n*5);
			b.setY(340);
			b.setDistance(1);
			barreVita[n]=b;
			scene.addActorToTheScene(b);
		}
		Input.addKeyListener(new KeyListener() {
			@Override
			public void notifyEvent(int key, int actionType) {
				currentState.notifyKeyPressed(key, actionType);
			}
		});
		Input.addMouseChangePositionListener(new MouseChangePositionListener() {
			
			@Override
			public void notifyNewPosition(double x, double y) {
				double deltaX=(x-lastMouseX);
				double deltaY=(y-lastMouseY);
				lastMouseX=x;
				lastMouseY=y;
				if (!(((MainScene)scene).theatre.getSpeed()==0)) {
					setX((float) (getX()+deltaX));
					setY((float) (getY()-deltaY));
					if (getX()>560) setX(560);
					else if (getX()<80) setX(80);
					if (getY()<40) setY(40);
					else if (getY()>300) setY(300);
				}
			}
		});
	}
	public void setSalute(int salute) {
		if (salute<0) salute=0;
		for (int n=0;n<salute;n++) 
			barreVita[n].setVisibility(true);
		for (int n=salute;n<12;n++)
			barreVita[n].setVisibility(false);
		//SE � STATO SUBITO UN DANNO ALLORA IL GIOCATORE DEVE LAMPEGGIARE
		if (this.salute>salute) 
			new Thread() {
				public void run() {
					for (int n=0;n<6;n++) {
						try {sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
						setVisibility(!getVisibility());
					}
				};
			}.start();
		this.salute = salute;
		
	}
	@Override
	public void act(float deltaTime) {
		currentState.act(deltaTime);
	}
	@Override
	public void notifyBeginCollision(Actor o) {
		if (o instanceof Asteroide || o instanceof Meteora || o instanceof NemicoAereo) {
			setSalute(salute-2);
		}
		else if (o instanceof BonusVita) {
			if (salute<12)
				setSalute(salute+1);
			new Thread() {
				public void run() {
					for (int n=0;n<8;n++) {
						try {sleep(50);} catch (InterruptedException e) {e.printStackTrace();}
						if (getColorToAdd()==Color.TRANSPARENT)
							setColorToAdd(new Color(-0.3f, 1, -0.3f, 0));
						else
							setColorToAdd(Color.TRANSPARENT);
					}
				};
			}.start();
		}
		if (salute<=0) {
			EmptyCircleActor cerchio=new EmptyCircleActor(25,new Color(1, 1, 0,1)) {
				
				@Override
				public void act(float deltaTime) {
					setScale(getScaleX()+20*deltaTime);
					setThickness(10);
				}
			};
			cerchio.setX(getX());
			cerchio.setY(getY());
			cerchio.setDistance(3);
			scene.addActorToTheScene(cerchio);
			scene.removeActorFromTheScene(this); 
			new Thread() {
				public void run() {
					for (int n=0;n<8;n++) {
						try {sleep(30);} catch (InterruptedException e) {e.printStackTrace();}
						if (scene.getBackgroundColor()==Color.BLACK)
							scene.setBackgroundColor(Color.WHITE);
						else
							scene.setBackgroundColor(Color.BLACK);
					}
				};
			}.start();
		}
	}
	
	public void setState(PlayerState newState) {
		currentState=newState;
	}
	public PlayerState getState() {
		return currentState;
	}
	class BarraVita extends SpriteActor{
		public BarraVita(String imageName) throws IOException {
			super(imageName);
		}

		@Override
		public void act(float deltaTime) {
		}
	}
	
	

	
}
