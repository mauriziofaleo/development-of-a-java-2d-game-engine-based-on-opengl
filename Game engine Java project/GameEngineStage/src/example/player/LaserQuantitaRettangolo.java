package example.player;

import Framework.Color;
import Framework.rendering.actors.RectangleActor;

public class LaserQuantitaRettangolo extends RectangleActor {
	Player player;
	public LaserQuantitaRettangolo(Player player) {
		super(56,10,Color.RED,Color.RED,Color.GREEN,Color.GREEN);
		setX(37);
		setY(320);
		this.player=player;
	}

	@Override
	public void act(float deltaTime) {
		if (player.currentState instanceof RaggioState && !getVisibility()) 
			setVisibility(true);
		else if (!(player.currentState instanceof RaggioState) && getVisibility())
			setVisibility(false);
		setWidth(56*RaggioState.laserQuantita);
		setX(9+getWidth()/2);
		
	}

}
