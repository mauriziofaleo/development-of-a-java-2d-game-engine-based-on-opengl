package example.player;

import java.io.IOException;

import Framework.colliders.RectangleCollider;
import Framework.input.Input;
import Framework.rendering.actors.SpriteActor;

public class RaggioState extends PlayerState{
	static Raggio raggio=null;
	public static float laserQuantita=0;
	public static PlayerState previousState;
	public RaggioState(Player player) {
		super(player);
		laserQuantita=1;
	}
	@Override
	public void act(float deltaTime) {
		if (laserQuantita<=0) {
			player.setState(previousState);
		}
	}
	@Override
	public void notifyKeyPressed(int key, int actionType) {
		if (key==Input.MOUSE_BUTTON_LEFT && actionType==Input.PRESS && raggio==null) {
			try {
				raggio=new Raggio(player);
				player.scene.addActorToTheScene(raggio);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public class Raggio extends SpriteActor {
		Player player;
		RectangleCollider collider=new RectangleCollider(6, 8, 0, 0);
		public Raggio(Player player) throws IOException {
			super("raggio.png");
			setX(player.getX());
			setY(player.getY()+15+(8f*getScaleY()/2));
			setScaleY(1);
			addCollider(collider);
			this.player=player;
		}

		@Override
		public void act(float deltaTime) {
			if (Input.getKey(Input.MOUSE_BUTTON_LEFT) && RaggioState.laserQuantita>0) {
				setScaleY(getScaleY()+300*deltaTime);
				if (getScaleY()>40) setScaleY(40);
				collider.setHeight(getScaleY()*8);
				setX(player.getX());
				setY(player.getY()+15+(8f*getScaleY()/2));
				RaggioState.laserQuantita-=deltaTime;
			}
			else {
				setScaleY(getScaleY()-300*deltaTime);
				if (getScaleY()<0) {
					RaggioState.raggio=null;
					player.scene.removeActorFromTheScene(this);
				}
				collider.setHeight(getScaleY()*8);
				setX(player.getX());
				setY(player.getY()+15+(8f*getScaleY()/2));
			}	
		}
	}

}
