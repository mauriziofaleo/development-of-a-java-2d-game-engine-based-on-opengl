package example.player;

import Framework.Color;
import Framework.rendering.actors.EmptyRectangleActor;

public class LaserQuantitaCornice extends EmptyRectangleActor {
	Player player;
	public LaserQuantitaCornice(Player player) {
		super(58,12,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE);
		setX(37);
		setY(320);
		this.player=player;
	}

	@Override
	public void act(float deltaTime) {
		if (player.currentState instanceof RaggioState && !getVisibility()) 
			setVisibility(true);
		else if (!(player.currentState instanceof RaggioState) && getVisibility())
			setVisibility(false);
	}

}
