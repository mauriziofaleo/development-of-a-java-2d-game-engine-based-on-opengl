package example;

import java.io.IOException;

import Framework.Color;
import Framework.Scene;
import Framework.colliders.CircleCollider;
import Framework.rendering.actors.EmptyCircleActor;
import Framework.rendering.actors.SpriteActor;
import Framework.rendering.interfaces.Actor;
import example.enemies.Asteroide;
import example.enemies.NemicoAereo;
import example.player.Proiettile;
import example.player.RaggioState;

public class ProiettileNemico extends SpriteActor {
	float speed=-200;
	Scene scene;
	public ProiettileNemico(float x,float y,Scene scene) throws IOException {
		super("proiettile_nemico.png");
		setX(x);
		setY(y);
		setDistance(2);
		addCollider(new CircleCollider(5, 0, 0));
		this.scene=scene;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void act(float deltaTime) {
		setY(getY()+speed*deltaTime);
		if (getY()<-10) {
			scene.removeActorFromTheScene(this);
		}
		
	}

	@Override
	public void notifyBeginCollision(Actor o) {
		if ((o instanceof Proiettile) || (o instanceof RaggioState.Raggio)) {
			EmptyCircleActor cerchio=new EmptyCircleActor(25,new Color(1, 1, 0,1)) {
				float alpha=0;
				@Override
				public void act(float deltaTime) {
					alpha-=3*deltaTime;
					if (alpha<-1) {
						scene.removeActorFromTheScene(this);
					}
					setScale(getScaleX()+20*deltaTime);
					setThickness(10);
					setColorToAdd(new Color(0, 0, 0, alpha));
				}
			};
			cerchio.setX(getX());
			cerchio.setY(getY());
			cerchio.setDistance(3);
			
			scene.addActorToTheScene(cerchio);
		}
		if (!(o instanceof Asteroide || o instanceof NemicoAereo)) {
			scene.removeActorFromTheScene(this);
		}
			
	}

	
}
