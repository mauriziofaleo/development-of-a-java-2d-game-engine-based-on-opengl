package example;

import java.io.IOException;
import Framework.Color;
import Framework.Scene;
import Framework.colliders.RectangleCollider;
import Framework.rendering.actors.CircleActor;
import Framework.rendering.actors.SpriteActor;
import Framework.rendering.interfaces.Actor;
import example.bonus.Bonus;

public class Parete extends SpriteActor{
	int salute=3;
	Scene scene;
	public Parete(Scene scene) throws IOException {
		super("wall.png");
		this.scene=scene;
		addCollider(new RectangleCollider(55, 18, 0, 0));
	}

	@Override
	public void act(float deltaTime) {
	}
	@Override
	public void notifyBeginCollision(Actor o) {
		if (!(o instanceof Bonus)) {
			salute--;
			setColorToAdd(new Color((1f/3)*(3-salute),(-1f/5)*(3-salute),(-1f/5)*(3-salute),0 ));
			if (salute==0) {
				esplodi(5);
				scene.removeActorFromTheScene(this);
			}
			new Thread() {
				public void run() {
					for (int n=0;n<6;n++) {
						try {sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
						setVisibility(!getVisibility());
					}
				};
			}.start();
		}
	}
	
	private void esplodi(int nroParticelle) {
		for (int n=0;n<nroParticelle;n++) {
			final int nFinal=n;
			CircleActor particle=new CircleActor(5,new Color(0.75f, 0.75f, 0.75f, 1),new Color(0.75f, 0.75f, 0.75f, 1)) {
				@Override
				public void act(float deltaTime) {
					setX((float) (getX()+deltaTime*500*Math.cos(Math.toRadians(120/(nroParticelle-1)*nFinal+30))));
					setY((float) (getY()+deltaTime*500*Math.sin(Math.toRadians(120/(nroParticelle-1)*nFinal+30))));
					if (getX()<-10 || getX()>660 || getY()<-10 || getY()>380 ) {
						scene.removeActorFromTheScene(this);
					}
					CircleActor scia=new CircleActor(5,new Color(0.75f, 0.75f, 0.75f, 0.5f),new Color(0.75f, 0.75f, 0.75f, 0.5f)) {
						
						@Override
						public void act(float deltaTime) {
							setScale(getScaleX()-6*deltaTime);
							if (getScaleX()<0)
								scene.removeActorFromTheScene(this);
						}
						
					};
					scia.setX(getX());
					scia.setY(getY());
					scene.addActorToTheScene(scia);
				}
			};
			particle.setX(getX());
			particle.setY(getY());
			scene.addActorToTheScene(particle);
		}
	}
	

}
