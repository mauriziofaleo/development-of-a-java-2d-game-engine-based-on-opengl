package example.bonus;


import java.io.IOException;
import Framework.Scene;
import Framework.colliders.CircleCollider;
import Framework.rendering.interfaces.Actor;
import example.player.Player;

public class BonusVita extends Bonus{
	public BonusVita(float x,float y,Scene scene) throws IOException {
		super("life_green.png",x,y,scene);
		addCollider(new CircleCollider(8, 0, 0));
	}

	@Override
	public void notifyBeginCollision(Actor o) {
		if (o instanceof Player) {
			scene.removeActorFromTheScene(this);
		}
	}
}
