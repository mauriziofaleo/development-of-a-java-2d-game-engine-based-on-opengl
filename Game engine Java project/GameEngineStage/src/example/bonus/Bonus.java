package example.bonus;

import java.io.IOException;


import Framework.Color;
import Framework.Scene;
import Framework.rendering.actors.SpriteActor;

public abstract class Bonus extends SpriteActor{
	float time=0;
	float speed=80;
	Scene scene;
	public Bonus(String imgName, float x,float y,Scene scene) throws IOException {
		super(imgName);
		setX(x);
		setY(y);
		setDistance(2);
		this.scene=scene;
	}

	@Override
	public void act(float deltaTime) {
		time+=3*deltaTime;
		float componentToAdd=(float) (0.6f*Math.abs(Math.sin(time)));
		setColorToAdd(new Color(componentToAdd,componentToAdd,componentToAdd, 0));
		setY(getY()-speed*deltaTime);
		if (getY()<-10) {
			scene.removeActorFromTheScene(this);
		}
	}
}
