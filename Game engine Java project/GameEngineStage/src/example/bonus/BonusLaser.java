package example.bonus;

import java.io.IOException;


import Framework.Scene;
import Framework.colliders.CircleCollider;
import Framework.rendering.interfaces.Actor;
import example.player.Player;
import example.player.RaggioState;

public class BonusLaser extends Bonus{
	public BonusLaser(float x,float y,Scene scene) throws IOException {
		super("raggio.png",x,y,scene);
		addCollider(new CircleCollider(8, 0, 0));
	}

	@Override
	public void notifyBeginCollision(Actor o) {
		if (o instanceof Player) {
			scene.removeActorFromTheScene(this);
			if (!(((Player)o).getState() instanceof RaggioState))
				RaggioState.previousState=((Player)o).getState();
			((Player)o).setState(new RaggioState((Player)o));
			
		}
	}
}
