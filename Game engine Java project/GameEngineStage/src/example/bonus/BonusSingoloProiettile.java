package example.bonus;

import java.io.IOException;


import Framework.Scene;
import Framework.colliders.CircleCollider;
import Framework.rendering.interfaces.Actor;
import example.player.Player;
import example.player.SingoloProiettileState;

public class BonusSingoloProiettile extends Bonus{
	public BonusSingoloProiettile(float x,float y,Scene scene) throws IOException {
		super("singolo_proiettile.png",x,y,scene);
		addCollider(new CircleCollider(8, 0, 0));
	}

	@Override
	public void notifyBeginCollision(Actor o) {
		if (o instanceof Player) {
			scene.removeActorFromTheScene(this);
			((Player)o).setState(new SingoloProiettileState((Player)o));
		}
	}
}
