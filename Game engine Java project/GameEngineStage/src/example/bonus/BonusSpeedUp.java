package example.bonus;


import java.io.IOException;
import Framework.Scene;
import Framework.colliders.CircleCollider;
import Framework.rendering.interfaces.Actor;
import example.MainScene;
import example.player.Player;

public class BonusSpeedUp extends Bonus{
	public BonusSpeedUp(float x,float y,Scene scene) throws IOException {
		super("speedUp.png",x,y,scene);
		setRotation(180);
		addCollider(new CircleCollider(8, 0, 0));
	}

	@Override
	public void notifyBeginCollision(Actor o) {
		if (o instanceof Player) {
			scene.removeActorFromTheScene(this);
			new Thread() {
				@Override
				public void run() {
					super.run();
					((MainScene)scene).theatre.setSpeed(2f);
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					((MainScene)scene).theatre.setSpeed(1);
				}
			}.start();
		}
	}
}
