package example.bonus;

import java.io.IOException;
import java.util.Random;

import example.MainScene;

public class BonusGenerator {
	public static MainScene scene;
	static Random random=new Random(); 
	public static void askForABonus(float x,float y) {
		int casual=random.nextInt(12);
		if (casual==0)
			try {scene.addActorToTheScene(new BonusSingoloProiettile(x, y, scene));} 
			catch (IOException e) {e.printStackTrace();}
		else if (casual==1)
			try {scene.addActorToTheScene(new BonusDoppioProiettile(x, y, scene));} 
			catch (IOException e) {e.printStackTrace();}
		else if (casual==2)
			try {scene.addActorToTheScene(new BonusTriploProiettile(x, y, scene));} 
			catch (IOException e) {e.printStackTrace();}
		else if (casual==3)
			try {scene.addActorToTheScene(new BonusSpeedUp(x, y, scene));} 
			catch (IOException e) {e.printStackTrace();}
		else if (casual==4)
			try {scene.addActorToTheScene(new BonusSpeedDown(x, y, scene));} 
			catch (IOException e) {e.printStackTrace();}
		else if (casual==5)
			try {scene.addActorToTheScene(new BonusVita(x, y, scene));} 
			catch (IOException e) {e.printStackTrace();}
		else if (casual==6)
			try {scene.addActorToTheScene(new BonusLaser(x, y, scene));} 
			catch (IOException e) {e.printStackTrace();}
		
	}
}
