package example.enemies;

import java.io.IOException;
import java.util.Random;
import Framework.Color;
import Framework.Scene;
import Framework.colliders.CircleCollider;
import Framework.input.Input;
import Framework.rendering.actors.CircleActor;
import Framework.rendering.actors.SpriteActor;
import Framework.rendering.interfaces.Actor;
import example.MainScene;
import example.Parete;
import example.ProiettileNemico;
import example.bonus.BonusGenerator;
import example.player.Player;
import example.player.Proiettile;
import example.player.RaggioState;

public class NemicoAereo extends SpriteActor {
	float speed=100;
	Scene scene;
	Random random=new Random();
	int salute=3;
	float timer=0.2f+random.nextFloat()*3;
	public NemicoAereo(Scene scene) throws IOException {
		super("nemico1.png");
		setDistance(5);
		setRotation(180);
		addCollider(new CircleCollider(16, 0, 0));
		setX(80+random.nextFloat()*480);
		setY(390);
		this.scene=scene;
	}

	@Override
	public void act(float deltaTime) {
		setY(getY()-speed*deltaTime);
		timer-=deltaTime;
		if (timer<0) {
			timer=0.2f+random.nextFloat()*3;
			try {
				scene.addActorToTheScene(new ProiettileNemico(getX(), getY()-25, scene));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (getY()<-30) {
			((MainScene)scene).theatre.setSpeed(0);
			if (Input.getKey(Input.MOUSE_BUTTON_LEFT)) {
				try {
					((MainScene)scene).theatre.setScene(new MainScene(((MainScene)scene).theatre));
					((MainScene)scene).theatre.setSpeed(1);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		else if (getY()<0) {
			((MainScene)scene).theatre.setSpeed(0.1f);
			scene.getCamera().setX((getX()-scene.getCamera().getX())*deltaTime*10+scene.getCamera().getX());
			scene.getCamera().setY((getY()-scene.getCamera().getY())*deltaTime*10+scene.getCamera().getY());
			scene.getCamera().setScale((2-scene.getCamera().getScale())*deltaTime*10+scene.getCamera().getScale());
			
		}
	}
	@Override
	public void notifyBeginCollision(Actor o) {
		if (o instanceof Proiettile) {
			salute--;
			setColorToAdd(new Color((1f/3)*(3-salute),(-1f/5)*(3-salute),(-1f/5)*(3-salute),0 ));
			if (salute==0) {
				scene.removeActorFromTheScene(this);
				esplodi(10);
			}
		}
		else if (o instanceof Parete) {
			scene.removeActorFromTheScene(this);
			esplodi(10);
		}
		else if (o instanceof Player) {
			scene.removeActorFromTheScene(this);
			esplodi(10);
		}
		else if (o instanceof RaggioState.Raggio) {
			scene.removeActorFromTheScene(this);
			esplodi(10);
		}
	}
	private void esplodi(int nroParticelle) {
		for (int n=0;n<nroParticelle;n++) {
			final int nFinal=n;
			Random random=new Random();
			CircleActor particle=new CircleActor(5,Color.RED,new Color(1, 1, 0, 1)) {
				float vitaParticella=0;
				int gravita=random.nextInt(1000);
				@Override
				public void act(float deltaTime) {
					vitaParticella+=deltaTime;
					setX((float) (getX()+deltaTime*500*Math.cos(Math.toRadians(360f/nroParticelle*nFinal))));
					setY((float) (getY()+deltaTime*500*Math.sin(Math.toRadians(360f/nroParticelle*nFinal))));
					setY(getY()-vitaParticella*gravita*deltaTime);
					if (getX()<-10 || getX()>660 || getY()<-10 || getY()>380 ) {
						scene.removeActorFromTheScene(this);
					}
					CircleActor scia=new CircleActor(5,new Color(1,0, 0,0.5f),new Color(1,0, 0,0.5f)) {
						
						@Override
						public void act(float deltaTime) {
							setScale(getScaleX()-6*deltaTime);
							if (getScaleX()<0) {
								scene.removeActorFromTheScene(this);
							}
						}
						
					};
					scia.setX(getX());
					scia.setY(getY());
					scia.setDistance(4);
					scene.addActorToTheScene(scia);
				}
			};
			particle.setX(getX());
			particle.setDistance(3);
			particle.setY(getY());
			scene.addActorToTheScene(particle);
		}
		BonusGenerator.askForABonus(getX(), getY());
	}
	

}
