package example.enemies;

import java.io.IOException;
import java.util.Random;

import Framework.Scene;
import Framework.rendering.actors.InvisibleActor;

public class EnemyGenerator extends InvisibleActor {
	Scene currentScene;
	float timer=1;
	Random random=new Random();
	float vita=100;
	public EnemyGenerator(Scene scene) {
		super();
		currentScene=scene;
	}
	@Override
	public void act(float deltaTime) {
		vita+=deltaTime;
		if (timer>0)
			timer-=deltaTime;
		else {
			try {
				int type=random.nextInt(3);
				if (type==0)
					currentScene.addActorToTheScene(new Asteroide(currentScene));
				else if (type==1)
					currentScene.addActorToTheScene(new NemicoAereo(currentScene));
				else 
					currentScene.addActorToTheScene(new Meteora(currentScene));
			} catch (IOException e) {
				e.printStackTrace();
			}
			timer=getTimer();
		}
	}
	float getTimer() {
		if (vita<30)
			return random.nextFloat()*2+0.5f;
		if (vita<60)
			return random.nextFloat()*1.5f+0.5f;
		if (vita<100)
			return random.nextFloat()*1+0.5f;
		if (vita<150)
			return random.nextFloat()*0.5f+0.5f;
		if (vita<200)
			return random.nextFloat()*0.5f;
		else
			return random.nextFloat()*0.3f;
	}

}
