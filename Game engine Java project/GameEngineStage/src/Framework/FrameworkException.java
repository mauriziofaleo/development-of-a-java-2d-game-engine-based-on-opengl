package Framework;

public class FrameworkException extends RuntimeException{
	private static final long serialVersionUID = 7420039255376259059L;
	public FrameworkException(String message) {
		super(message);
	}
}
