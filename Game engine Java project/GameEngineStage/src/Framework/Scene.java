package Framework;


import java.util.ArrayList;
import Framework.colliders.Collider;
import Framework.colliders.CollisionManager;
import Framework.rendering.interfaces.Actor;
import Framework.rendering.interfaces.Renderable;

public class Scene {
	//ATTRIBUTES
	private Color backgroundColor=Color.BLACK;
	ArrayList<Actor> listActors=new ArrayList<Actor>();
	ArrayList<Actor> listActorsToAdd=new ArrayList<Actor>();
	ArrayList<Actor> listActorsToRemove=new ArrayList<Actor>();
	CollisionManager collisionManager=new CollisionManager();
	public Camera camera;
	//CONSTRUCTORS
	public Scene() {}
	public Scene(Color backgroundColor) {
		setBackgroundColor(backgroundColor);
	}
	//METHODS
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor=backgroundColor;
	}
	public Color getBackgroundColor() {
		return backgroundColor;
	}
	public void addActorToTheScene(Actor actorObject) {
		listActorsToAdd.add(actorObject);
	}
	public void removeActorFromTheScene(Actor actorObject) {
		listActorsToRemove.add(actorObject);
	}
	public void activateCollidersRendering() {
		collisionManager.activateRendering();
	}
	public void deactivateCollidersRendering() {
		collisionManager.deactivateRendering();
	}
	void render() {
		for (Actor actor:listActors) {
			if (actor instanceof Renderable) {
				((Renderable)actor).render();
			}
		}
		if (collisionManager.isRenderingEnabled())
			for (Collider c:collisionManager.listColliders) {
				c.render();
			}
	}
	
	void act(float deltaTime) {
		for (Actor actor:listActors) {
			actor.act(deltaTime);
		}
		for (Actor actor:listActorsToAdd) {
			listActors.add(actor);
			for (Collider c:actor.getColliders())
				collisionManager.listColliders.add(c);
		}
		listActorsToAdd.clear();
		for (Actor actor:listActorsToRemove) {
			listActors.remove(actor);
			for (Collider c:actor.getColliders())
				collisionManager.listColliders.remove(c);
		}
		listActorsToRemove.clear();
		for (Collider c:collisionManager.listColliders) {
			c.align();
		}
		collisionManager.checkCollision();
	}
	
	public void setCamera(Camera cam) {
		camera=cam;
	}
	
	public Camera getCamera() {
		return camera;
	}
}
