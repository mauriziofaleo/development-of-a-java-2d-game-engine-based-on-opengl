	package Framework;


public class Color {
	//STATIC ATTRIBUTES
	public static final Color TRANSPARENT=new Color(0, 0, 0, 0);
	public static final Color BLACK=new Color(0, 0, 0, 1);
	public static final Color GREEN=new Color(0, 1, 0, 1);
	public static final Color BLUE=new Color(0, 0, 1, 1);
	public static final Color RED=new Color(1, 0, 0, 1);
	public static final Color WHITE=new Color(1, 1, 1, 1);
	//ATTRIBUTES
	float red,green,blue,opacity;
	//CONSTRUCTORS
	public Color(float red,float green,float blue,float opacity) {
		this.red=red;
		this.green=green;
		this.blue=blue;
		this.opacity=opacity;
	}
	//METHODS
	public float[] getRGBA() {
		return new float[] {red,green,blue,opacity};
	}
	public float[] getRGB() {
        return new float[] {red,green,blue};
    }
	public void setOpacity(float opacity) {
		this.opacity=opacity;
	}
	public void setRed(float red) {
		this.red=red;
	}
	public void setGreen(float green) {
		this.green=green;
	}
	public void setBlue(float blue) {
		this.blue=blue;
	}
	public float getRed() {
		return red;
	}
	public float getGreen() {
		return green;
	}
	public float getBlue() {
		return blue;
	}
	public float getOpacity() {
		return opacity;
	}
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Color)) {
            return false;
        }
		Color color=(Color)obj;
		return (red==color.red && green==color.green && blue==color.blue && opacity==color.opacity);
	}
}
