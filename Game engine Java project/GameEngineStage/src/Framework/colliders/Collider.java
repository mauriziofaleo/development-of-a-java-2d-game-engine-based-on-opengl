package Framework.colliders;

import Framework.Color;
import Framework.rendering.interfaces.Actor;
import Framework.rendering.interfaces.Renderable;

public abstract class Collider {
    private static int idGen = 0;
    int offsetX = 0, offsetY = 0;
    private int id;
    Actor owner;
    Renderable renderableCollider;
    private boolean enabled = true;

    public Collider() {
        id = genId();
    }

    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }

    public void setOffsetY(int offsetY) {
        this.offsetY = offsetY;
    }

    public int getColliderId() {
        return id;
    }

    public Actor getOwner() {
        return owner;
    }

    public void setOwner(Actor owner) {
        this.owner = owner;
    }

    public void align() {
        renderableCollider.setX(owner.getX() + offsetX);
        renderableCollider.setY(owner.getY() + offsetY);
    }

    public int getX() {
        return (int) renderableCollider.getX();
    }

    public int getY() {
        return (int) renderableCollider.getY();
    }

    public void disableCollider() {
        enabled = false;
        renderableCollider.setColorToAdd(new Color(-0.25f, 0.75f, 0.75f, 0));
    }

    public void enableCollider() {
        enabled = true;
        renderableCollider.setColorToAdd(Color.TRANSPARENT);
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void render() {
        renderableCollider.render();
    }

    private static int genId() {
        idGen++;
        return idGen;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Collider)) {
            return false;
        }
        Collider collider = (Collider) obj;
        return getColliderId() == collider.getColliderId();
    }
}
