package Framework.colliders;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Objects;

public class CollisionManager {
    private boolean renderingEnabled = false;

    static class Collision<F extends Collider, S extends Collider> {
        public final F first;
        public final S second;

        public Collision(F first, S second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public int hashCode() {
            return Integer.hashCode(first.getColliderId() * 11 + second.getColliderId());
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Collision)) {
                return false;
            }
            Collision<?, ?> p = (Collision<?, ?>) o;
            return Objects.equals(p.first, first) && Objects.equals(p.second, second);
        }
    }
    
    Hashtable<Collision<Collider, Collider>, Boolean> collisionEvents = new Hashtable<Collision<Collider, Collider>, Boolean>();
    public ArrayList<Collider> listColliders = new ArrayList<Collider>();

    public boolean isRenderingEnabled() {
        return renderingEnabled;
    }

    public void activateRendering() {
        renderingEnabled = true;
    }

    public void deactivateRendering() {
        renderingEnabled = false;
    }

    public void checkCollision() {
        for (int n = 0; n < listColliders.size(); n++) {
            Collider c1 = listColliders.get(n);
            if (c1.isEnabled()) {
                for (int m = n + 1; m < listColliders.size(); m++) {
                    Collider c2 = listColliders.get(m);
                    if (c2.isEnabled()) {
                        Collision<Collider, Collider> pair = new Collision<Collider, Collider>(c1, c2);
                        if (!collisionEvents.containsKey(pair)) {
                            collisionEvents.put(pair, false);
                        }
                        boolean result = false;
                        if (c1 instanceof CircleCollider && c2 instanceof CircleCollider)
                            result = checkCollision((CircleCollider) c1, (CircleCollider) c2);
                        else if (c1 instanceof CircleCollider && c2 instanceof RectangleCollider)
                            result = checkCollision((CircleCollider) c1, (RectangleCollider) c2);
                        else if (c1 instanceof RectangleCollider && c2 instanceof CircleCollider)
                            result = checkCollision((RectangleCollider) c1, (CircleCollider) c2);
                        else if (c1 instanceof RectangleCollider && c2 instanceof RectangleCollider)
                            result = checkCollision((RectangleCollider) c1, (RectangleCollider) c2);
                        if (result) {
                            if (!collisionEvents.get(pair)) {
                                collisionEvents.put(pair, true);
                                c1.owner.notifyBeginCollision(c2.owner);
                                c2.owner.notifyBeginCollision(c1.owner);
                            } else {
                                c1.owner.notifyCollision(c2.owner);
                                c2.owner.notifyCollision(c1.owner);
                            }
                        } else {
                            if (collisionEvents.get(pair)) {
                                collisionEvents.put(pair, false);
                                c1.owner.notifyEndCollision(c2.owner);
                                c2.owner.notifyEndCollision(c1.owner);
                            }
                        }
                    }
                }
            }
        }
    }

    boolean checkCollision(CircleCollider c1, CircleCollider c2) {
        float distance = (float) Math.sqrt(Math.pow(c1.getX() - c2.getX(), 2) + Math.pow(c1.getY() - c2.getY(), 2));
        if (distance < c1.radius + c2.radius)
            return true;
        else
            return false;
    }

    boolean checkCollision(CircleCollider c1, RectangleCollider c2) {
        int Xdistance = Math.abs(c1.getX() - c2.getX());
        int Ydistance = Math.abs(c1.getY() - c2.getY());
        if (Xdistance > (c2.width / 2 + c1.radius)) {
            return false;
        }
        if (Ydistance > (c2.height / 2 + c1.radius)) {
            return false;
        }
        if (Xdistance <= (c2.width / 2)) {
            return true;
        }
        if (Ydistance <= (c2.height / 2)) {
            return true;
        }
        double distanzaAnngoloRettangolo = Math.pow((Xdistance - c2.width / 2), 2)
                + Math.pow((Ydistance - c2.height / 2), 2);
        return (distanzaAnngoloRettangolo <= Math.pow(c1.radius, 2));
    }

    boolean checkCollision(RectangleCollider c1, CircleCollider c2) {
        return checkCollision(c2, c1);
    }

    boolean checkCollision(RectangleCollider c1, RectangleCollider c2) {
        int Xdistance = Math.abs(c1.getX() - c2.getX());
        int Ydistance = Math.abs(c1.getY() - c2.getY());
        if (Xdistance > (c1.width / 2 + c2.width / 2)) {
            return false;
        }
        if (Ydistance > (c1.height / 2 + c2.height / 2)) {
            return false;
        }
        return true;
    }

}
