package Framework.colliders;

import Framework.Color;
import Framework.rendering.actors.EmptyRectangleActor;

public class RectangleCollider extends Collider {
    float width, height;

    public RectangleCollider(int width, int height, int offsetX, int offsetY) {
        super();
        this.width = width;
        this.height = height;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        renderableCollider = new EmptyRectangleActor(width, height, Color.RED, Color.RED, Color.RED, Color.RED) {
            @Override
            public void act(float deltaTime) {}
        };
        renderableCollider.setDistance(1f / Long.MAX_VALUE);
    }

    public void setWidth(float width) {
        ((EmptyRectangleActor) renderableCollider).setWidth(width);
        this.width = width;
    }

    public void setHeight(float height) {
        ((EmptyRectangleActor) renderableCollider).setHeight(height);
        this.height = height;
    }
}
