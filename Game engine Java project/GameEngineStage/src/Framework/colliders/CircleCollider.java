package Framework.colliders;

import Framework.Color;
import Framework.rendering.actors.EmptyCircleActor;

public class CircleCollider extends Collider {
    float radius;

    public CircleCollider(int radius, int offsetX, int offsetY) {
        super();
        this.radius = radius;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        renderableCollider = new EmptyCircleActor(radius, Color.RED) {
            @Override
            public void act(float deltaTime) {

            }
        };
        renderableCollider.setDistance(1f / Long.MAX_VALUE);
    }

    public void setRaggio(float radius) {
        ((EmptyCircleActor) renderableCollider).setRadius(radius);
        this.radius = radius;
    }
}
