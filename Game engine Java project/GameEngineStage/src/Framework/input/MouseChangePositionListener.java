package Framework.input;

public abstract class MouseChangePositionListener {
	public abstract void notifyNewPosition(double x,double y);
}
