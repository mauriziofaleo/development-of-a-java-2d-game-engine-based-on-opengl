package Framework.input;

import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;

import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;

public class Input {
    // STATIC INPUTE KEYS
    public static final int RELEASE = GLFW_RELEASE;
    public static final int PRESS = GLFW_PRESS;
    public static final int REPEAT = GLFW_REPEAT;
    public static final int HAT_CENTERED = GLFW_HAT_CENTERED;
    public static final int HAT_UP = GLFW_HAT_UP;
    public static final int HAT_RIGHT = GLFW_HAT_RIGHT;
    public static final int HAT_DOWN = GLFW_HAT_DOWN;
    public static final int HAT_LEFT = GLFW_HAT_LEFT;
    public static final int HAT_RIGHT_UP = GLFW_HAT_RIGHT_UP;
    public static final int HAT_RIGHT_DOWN = GLFW_HAT_RIGHT_DOWN;
    public static final int HAT_LEFT_UP = GLFW_HAT_LEFT_UP;
    public static final int HAT_LEFT_DOWN = GLFW_HAT_LEFT_DOWN;
    public static final int KEY_UNKNOWN = GLFW_KEY_UNKNOWN;
    public static final int KEY_SPACE = GLFW_KEY_SPACE;
    public static final int KEY_APOSTROPHE = GLFW_KEY_APOSTROPHE;
    public static final int KEY_COMMA = GLFW_KEY_COMMA;
    public static final int KEY_MINUS = GLFW_KEY_MINUS;
    public static final int KEY_PERIOD = GLFW_KEY_PERIOD;
    public static final int KEY_SLASH = GLFW_KEY_SLASH;
    public static final int KEY_0 = GLFW_KEY_0;
    public static final int KEY_1 = GLFW_KEY_1;
    public static final int KEY_2 = GLFW_KEY_2;
    public static final int KEY_3 = GLFW_KEY_3;
    public static final int KEY_4 = GLFW_KEY_4;
    public static final int KEY_5 = GLFW_KEY_5;
    public static final int KEY_6 = GLFW_KEY_6;
    public static final int KEY_7 = GLFW_KEY_7;
    public static final int KEY_8 = GLFW_KEY_8;
    public static final int KEY_9 = GLFW_KEY_9;
    public static final int KEY_SEMICOLON = GLFW_KEY_SEMICOLON;
    public static final int KEY_EQUAL = GLFW_KEY_EQUAL;
    public static final int KEY_A = GLFW_KEY_A;
    public static final int KEY_B = GLFW_KEY_B;
    public static final int KEY_C = GLFW_KEY_C;
    public static final int KEY_D = GLFW_KEY_D;
    public static final int KEY_E = GLFW_KEY_E;
    public static final int KEY_F = GLFW_KEY_F;
    public static final int KEY_G = GLFW_KEY_G;
    public static final int KEY_H = GLFW_KEY_H;
    public static final int KEY_I = GLFW_KEY_I;
    public static final int KEY_J = GLFW_KEY_J;
    public static final int KEY_K = GLFW_KEY_K;
    public static final int KEY_L = GLFW_KEY_L;
    public static final int KEY_M = GLFW_KEY_M;
    public static final int KEY_N = GLFW_KEY_N;
    public static final int KEY_O = GLFW_KEY_O;
    public static final int KEY_P = GLFW_KEY_P;
    public static final int KEY_Q = GLFW_KEY_Q;
    public static final int KEY_R = GLFW_KEY_R;
    public static final int KEY_S = GLFW_KEY_S;
    public static final int KEY_T = GLFW_KEY_T;
    public static final int KEY_U = GLFW_KEY_U;
    public static final int KEY_V = GLFW_KEY_V;
    public static final int KEY_W = GLFW_KEY_W;
    public static final int KEY_X = GLFW_KEY_X;
    public static final int KEY_Y = GLFW_KEY_Y;
    public static final int KEY_Z = GLFW_KEY_Z;
    public static final int KEY_LEFT_BRACKET = GLFW_KEY_LEFT_BRACKET;
    public static final int KEY_BACKSLASH = GLFW_KEY_BACKSLASH;
    public static final int KEY_RIGHT_BRACKET = GLFW_KEY_RIGHT_BRACKET;
    public static final int KEY_GRAVE_ACCENT = GLFW_KEY_GRAVE_ACCENT;
    public static final int KEY_WORLD_1 = GLFW_KEY_WORLD_1;
    public static final int KEY_WORLD_2 = GLFW_KEY_WORLD_2;
    public static final int KEY_ESCAPE = GLFW_KEY_ESCAPE;
    public static final int KEY_ENTER = GLFW_KEY_ENTER;
    public static final int KEY_TAB = GLFW_KEY_TAB;
    public static final int KEY_BACKSPACE = GLFW_KEY_BACKSPACE;
    public static final int KEY_INSERT = GLFW_KEY_INSERT;
    public static final int KEY_DELETE = GLFW_KEY_DELETE;
    public static final int KEY_RIGHT = GLFW_KEY_RIGHT;
    public static final int KEY_LEFT = GLFW_KEY_LEFT;
    public static final int KEY_DOWN = GLFW_KEY_DOWN;
    public static final int KEY_UP = GLFW_KEY_UP;
    public static final int KEY_PAGE_UP = GLFW_KEY_PAGE_UP;
    public static final int KEY_PAGE_DOWN = GLFW_KEY_PAGE_DOWN;
    public static final int KEY_HOME = GLFW_KEY_HOME;
    public static final int KEY_END = GLFW_KEY_END;
    public static final int KEY_CAPS_LOCK = GLFW_KEY_CAPS_LOCK;
    public static final int KEY_SCROLL_LOCK = GLFW_KEY_SCROLL_LOCK;
    public static final int KEY_NUM_LOCK = GLFW_KEY_NUM_LOCK;
    public static final int KEY_PRINT_SCREEN = GLFW_KEY_PRINT_SCREEN;
    public static final int KEY_PAUSE = GLFW_KEY_PAUSE;
    public static final int KEY_F1 = GLFW_KEY_F1;
    public static final int KEY_F2 = GLFW_KEY_F2;
    public static final int KEY_F3 = GLFW_KEY_F3;
    public static final int KEY_F4 = GLFW_KEY_F4;
    public static final int KEY_F5 = GLFW_KEY_F5;
    public static final int KEY_F6 = GLFW_KEY_F6;
    public static final int KEY_F7 = GLFW_KEY_F7;
    public static final int KEY_F8 = GLFW_KEY_F8;
    public static final int KEY_F9 = GLFW_KEY_F9;
    public static final int KEY_F10 = GLFW_KEY_F10;
    public static final int KEY_F11 = GLFW_KEY_F11;
    public static final int KEY_F12 = GLFW_KEY_F12;
    public static final int KEY_F13 = GLFW_KEY_F13;
    public static final int KEY_F14 = GLFW_KEY_F14;
    public static final int KEY_F15 = GLFW_KEY_F15;
    public static final int KEY_F16 = GLFW_KEY_F16;
    public static final int KEY_F17 = GLFW_KEY_F17;
    public static final int KEY_F18 = GLFW_KEY_F18;
    public static final int KEY_F19 = GLFW_KEY_F19;
    public static final int KEY_F20 = GLFW_KEY_F20;
    public static final int KEY_F21 = GLFW_KEY_F21;
    public static final int KEY_F22 = GLFW_KEY_F22;
    public static final int KEY_F23 = GLFW_KEY_F23;
    public static final int KEY_F24 = GLFW_KEY_F24;
    public static final int KEY_F25 = GLFW_KEY_F25;
    public static final int KEY_KP_0 = GLFW_KEY_KP_0;
    public static final int KEY_KP_1 = GLFW_KEY_KP_1;
    public static final int KEY_KP_2 = GLFW_KEY_KP_2;
    public static final int KEY_KP_3 = GLFW_KEY_KP_3;
    public static final int KEY_KP_4 = GLFW_KEY_KP_4;
    public static final int KEY_KP_5 = GLFW_KEY_KP_5;
    public static final int KEY_KP_6 = GLFW_KEY_KP_6;
    public static final int KEY_KP_7 = GLFW_KEY_KP_7;
    public static final int KEY_KP_8 = GLFW_KEY_KP_8;
    public static final int KEY_KP_9 = GLFW_KEY_KP_9;
    public static final int KEY_KP_DECIMAL = GLFW_KEY_KP_DECIMAL;
    public static final int KEY_KP_DIVIDE = GLFW_KEY_KP_DIVIDE;
    public static final int KEY_KP_MULTIPLY = GLFW_KEY_KP_MULTIPLY;
    public static final int KEY_KP_SUBTRACT = GLFW_KEY_KP_SUBTRACT;
    public static final int KEY_KP_ADD = GLFW_KEY_KP_ADD;
    public static final int KEY_KP_ENTER = GLFW_KEY_KP_ENTER;
    public static final int KEY_KP_EQUAL = GLFW_KEY_KP_EQUAL;
    public static final int KEY_LEFT_SHIFT = GLFW_KEY_LEFT_SHIFT;
    public static final int KEY_LEFT_CONTROL = GLFW_KEY_LEFT_CONTROL;
    public static final int KEY_LEFT_ALT = GLFW_KEY_LEFT_ALT;
    public static final int KEY_LEFT_SUPER = GLFW_KEY_LEFT_SUPER;
    public static final int KEY_RIGHT_SHIFT = GLFW_KEY_RIGHT_SHIFT;
    public static final int KEY_RIGHT_CONTROL = GLFW_KEY_RIGHT_CONTROL;
    public static final int KEY_RIGHT_ALT = GLFW_KEY_RIGHT_ALT;
    public static final int KEY_RIGHT_SUPER = GLFW_KEY_RIGHT_SUPER;
    public static final int KEY_MENU = GLFW_KEY_MENU;
    public static final int KEY_LAST = GLFW_KEY_LAST;
    public static final int MOD_SHIFT = GLFW_MOD_SHIFT;
    public static final int MOD_CONTROL = GLFW_MOD_CONTROL;
    public static final int MOD_ALT = GLFW_MOD_ALT;
    public static final int MOD_SUPER = GLFW_MOD_SUPER;
    public static final int MOUSE_BUTTON_1 = GLFW_MOUSE_BUTTON_1;
    public static final int MOUSE_BUTTON_2 = GLFW_MOUSE_BUTTON_2;
    public static final int MOUSE_BUTTON_3 = GLFW_MOUSE_BUTTON_3;
    public static final int MOUSE_BUTTON_4 = GLFW_MOUSE_BUTTON_4;
    public static final int MOUSE_BUTTON_5 = GLFW_MOUSE_BUTTON_5;
    public static final int MOUSE_BUTTON_6 = GLFW_MOUSE_BUTTON_6;
    public static final int MOUSE_BUTTON_7 = GLFW_MOUSE_BUTTON_7;
    public static final int MOUSE_BUTTON_8 = GLFW_MOUSE_BUTTON_8;
    public static final int MOUSE_BUTTON_LAST = GLFW_MOUSE_BUTTON_LAST;
    public static final int MOUSE_BUTTON_LEFT = GLFW_MOUSE_BUTTON_LEFT;
    public static final int MOUSE_BUTTON_RIGHT = GLFW_MOUSE_BUTTON_RIGHT;
    public static final int MOUSE_BUTTON_MIDDLE = GLFW_MOUSE_BUTTON_MIDDLE;
    public static final int JOYSTICK_1 = GLFW_JOYSTICK_1;
    public static final int JOYSTICK_2 = GLFW_JOYSTICK_2;
    public static final int JOYSTICK_3 = GLFW_JOYSTICK_3;
    public static final int JOYSTICK_4 = GLFW_JOYSTICK_4;
    public static final int JOYSTICK_5 = GLFW_JOYSTICK_5;
    public static final int JOYSTICK_6 = GLFW_JOYSTICK_6;
    public static final int JOYSTICK_7 = GLFW_JOYSTICK_7;
    public static final int JOYSTICK_8 = GLFW_JOYSTICK_8;
    public static final int JOYSTICK_9 = GLFW_JOYSTICK_9;
    public static final int JOYSTICK_10 = GLFW_JOYSTICK_10;
    public static final int JOYSTICK_11 = GLFW_JOYSTICK_11;
    public static final int JOYSTICK_12 = GLFW_JOYSTICK_12;
    public static final int JOYSTICK_13 = GLFW_JOYSTICK_13;
    public static final int JOYSTICK_14 = GLFW_JOYSTICK_14;
    public static final int JOYSTICK_15 = GLFW_JOYSTICK_15;
    public static final int JOYSTICK_16 = GLFW_JOYSTICK_16;
    public static final int JOYSTICK_LAST = GLFW_JOYSTICK_LAST;
    private static boolean keyListenersEnabled = true;
    private static boolean mouseChangePositionListenersEnabled = true;
    // STATIC ATTRIBUTES
    private static ArrayList<KeyListener> keyListeners = new ArrayList<KeyListener>();
    private static ArrayList<MouseChangePositionListener> mouseChangePositionListeners = new ArrayList<MouseChangePositionListener>();
 
    static {
        glfwSetKeyCallback(glfwGetCurrentContext(), new GLFWKeyCallback() {
            @Override
            public void invoke(long arg0, int arg1, int arg2, int arg3, int arg4) {
                if (keyListenersEnabled)
                    for (KeyListener k : keyListeners)
                        k.notifyEvent(arg1, arg3);
            }
        });
        glfwSetMouseButtonCallback(glfwGetCurrentContext(), new GLFWMouseButtonCallback() {
            @Override
            public void invoke(long arg0, int arg1, int arg2, int arg3) {
                if (keyListenersEnabled)
                    for (KeyListener k : keyListeners)
                        k.notifyEvent(arg1, arg2);
            }
        });
        glfwSetCursorPosCallback(glfwGetCurrentContext(), new GLFWCursorPosCallback() {
            @Override
            public void invoke(long arg0, double arg1, double arg2) {
                if (mouseChangePositionListenersEnabled)
                    for (MouseChangePositionListener k : mouseChangePositionListeners)
                        k.notifyNewPosition(arg1, arg2);
            }

        });
    }
    
    // METHODS
    public static void addKeyListener(KeyListener keyListener) {
        keyListeners.add(keyListener);
    }

    public static void removeKeyListener(KeyListener keyListener) {
        keyListeners.remove(keyListener);
    }

    public static void addMouseChangePositionListener(MouseChangePositionListener mouseChangePositionListener) {
        mouseChangePositionListeners.add(mouseChangePositionListener);
    }

    public static void removeMouseChangePositionListener(MouseChangePositionListener mouseChangePositionListener) {
        mouseChangePositionListeners.remove(mouseChangePositionListener);
    }

    public static boolean getKey(int key) {
        return (glfwGetKey(glfwGetCurrentContext(), key) == 1)
                || (glfwGetMouseButton(glfwGetCurrentContext(), key) == 1);
    }

    public static void disableKeyListeners() {
        keyListenersEnabled = false;
    }

    public static void enableKeyListeners() {
        keyListenersEnabled = true;
    }

    public static void disableMouseChangePositionListeners() {
        mouseChangePositionListenersEnabled = false;
    }

    public static void enableMouseChangePositionListeners() {
        mouseChangePositionListenersEnabled = true;
    }

    public static void showeMouseCursor() {
        glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }

    public static void hideMouseCursor() {
        glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }

    public static void setMousePosition(double x, double y) {
        glfwSetCursorPos(glfwGetCurrentContext(), x, y);
    }
}
