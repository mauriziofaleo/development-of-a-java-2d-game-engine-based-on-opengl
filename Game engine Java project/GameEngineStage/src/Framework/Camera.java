package Framework;

import Framework.rendering.shaders.Circle_ShadersProgram;
import Framework.rendering.shaders.EmptyCircle_ShadersProgram;
import Framework.rendering.shaders.RectangleLine_ShadersProgram;
import Framework.rendering.shaders.Sprite_ShadersProgram;

public class Camera {
    // ATTRIBUTES
    private int width, height;
    private float x, y, angle, scale;
    final int Znear = 0;
    final int Zfar = 10;

    // CONSTRUCTORS
    public Camera(int widthViewPort, int heightViewPort) {
        this.width = widthViewPort;
        this.height = heightViewPort;
        this.x = 0;
        this.y = 0;
        this.angle = 0;
        this.scale = 1;
        updateTransformationMatrixCamera();
    }

    // METHODS
    public void setX(float x) {
        this.x = x;
        updateTransformationMatrixCamera();
    }

    public void setY(float y) {
        this.y = y;
        updateTransformationMatrixCamera();
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setRotation(float angle) {
        this.angle = angle;
        updateTransformationMatrixCamera();
    }

    public float getRotation() {
        return angle;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setScale(float scale) {
        this.scale = scale;
        updateTransformationMatrixCamera();
    }

    public float getScale() {
        return scale;
    }

    void updateTransformationMatrixCamera() {
        Sprite_ShadersProgram.shaderInstance.bind();
        Sprite_ShadersProgram.shaderInstance.setTransformationMatrixCamera(-x, -y, angle, scale);
        Sprite_ShadersProgram.shaderInstance.unbind();
        RectangleLine_ShadersProgram.shaderInstance.bind();
        RectangleLine_ShadersProgram.shaderInstance.setTransformationMatrixCamera(-x, -y, angle, scale);
        RectangleLine_ShadersProgram.shaderInstance.unbind();
        EmptyCircle_ShadersProgram.shaderInstance.bind();
        EmptyCircle_ShadersProgram.shaderInstance.setTransformationMatrixCamera(-x, -y, angle, scale);
        EmptyCircle_ShadersProgram.shaderInstance.unbind();
        Circle_ShadersProgram.shaderInstance.bind();
        Circle_ShadersProgram.shaderInstance.setTransformationMatrixCamera(-x, -y, angle, scale);
        Circle_ShadersProgram.shaderInstance.unbind();
    }

    public void updateOrtographicProjectionMatrix() {
        Sprite_ShadersProgram.shaderInstance.bind();
        Sprite_ShadersProgram.shaderInstance.setOrtographicProjectionMatrix(getWidth(), getHeight(), Znear, Zfar);
        Sprite_ShadersProgram.shaderInstance.unbind();
        RectangleLine_ShadersProgram.shaderInstance.bind();
        RectangleLine_ShadersProgram.shaderInstance.setOrtographicProjectionMatrix(getWidth(), getHeight(), Znear,
                Zfar);
        RectangleLine_ShadersProgram.shaderInstance.unbind();
        EmptyCircle_ShadersProgram.shaderInstance.bind();
        EmptyCircle_ShadersProgram.shaderInstance.setOrtographicProjectionMatrix(getWidth(), getHeight(), Znear, Zfar);
        EmptyCircle_ShadersProgram.shaderInstance.unbind();
        Circle_ShadersProgram.shaderInstance.bind();
        Circle_ShadersProgram.shaderInstance.setOrtographicProjectionMatrix(getWidth(), getHeight(), Znear, Zfar);
        Circle_ShadersProgram.shaderInstance.unbind();
    }

}
