package Framework.rendering.interfaces;

import Framework.Color;
import Framework.FrameworkException;

public abstract class Renderable extends Actor {
    protected float distance = 5;
    protected float rotation = 0;
    protected float scaleX = 1;
    protected float scaleY = 1;
    protected Color colorToAdd = Color.TRANSPARENT;
    protected boolean visible = true;

    public void setDistance(float distance) {
        if (distance >= 10 || distance <= 0) {
            throw new FrameworkException("Distance must be between 0(exclusive) and 10 (exclusive)");
        }
        this.distance = distance;
    }

    public float getDistance() {
        return distance;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public float getRotation() {
        return rotation;
    }

    public void setScale(float scale) {
        this.scaleX = scale;
        this.scaleY = scale;
    }

    public void setScaleX(float scale) {
        this.scaleX = scale;
    }

    public void setScaleY(float scale) {
        this.scaleY = scale;
    }

    public float getScaleX() {
        return scaleX;
    }

    public float getScaleY() {
        return scaleY;
    }

    public void setVisibility(boolean visible) {
        this.visible = visible;
    }

    public boolean getVisibility() {
        return visible;
    }

    public void setColorToAdd(Color colorToAdd) {
        this.colorToAdd = colorToAdd;
    }

    public Color getColorToAdd() {
        return colorToAdd;
    }

    public abstract void render();    
}
