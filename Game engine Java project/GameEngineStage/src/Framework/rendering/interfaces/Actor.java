package Framework.rendering.interfaces;

import java.util.ArrayList;

import Framework.colliders.Collider;

public abstract class Actor {
    protected float x = 0, y = 0;
    protected ArrayList<Collider> colliders = new ArrayList<Collider>();
    public void setX(float x) {
        this.x = x;
    }

    public float getX() {
        return x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getY() {
        return y;
    }

    public void setPosition(float x, float y) {
        setX(x);
        setY(y);
    }

    public void addCollider(Collider collider) {
        collider.setOwner(this);
        this.colliders.add(collider);
    }

    public void removeCollider(Collider collider) {
        this.colliders.remove(collider);
    }

    public ArrayList<Collider> getColliders() {
        return this.colliders;
    }

    public abstract void act(float deltaTime);

    public void notifyBeginCollision(Actor o) {
    }

    public void notifyCollision(Actor o) {
    }

    public void notifyEndCollision(Actor o) {
    }
}
