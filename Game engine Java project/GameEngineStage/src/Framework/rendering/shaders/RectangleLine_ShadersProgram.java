package Framework.rendering.shaders;

public class RectangleLine_ShadersProgram extends ShadersProgram {
    public static RectangleLine_ShadersProgram shaderInstance;

    public RectangleLine_ShadersProgram() {
        createProgram("Rectangle_Line_Shader.vs", "Rectangle_Line_Shader.fs");
        shaderInstance = this;
    }
}
