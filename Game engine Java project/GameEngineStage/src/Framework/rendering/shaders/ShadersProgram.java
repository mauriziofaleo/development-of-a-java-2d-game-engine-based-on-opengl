package Framework.rendering.shaders;

import static org.lwjgl.opengl.GL20.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;
import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;

import Framework.Color;

public abstract class ShadersProgram {
    int vShader;
    int fShader;
    int program;

    protected String readFile(String filename) {
        StringBuilder string = new StringBuilder();
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(new File("./shaders/" + filename)));
            String line;
            while ((line = br.readLine()) != null) {
                string.append(line);
                string.append("\n");
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return string.toString();
    }

    public void createProgram(String vertexShader, String fragmentShader) {
        vShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vShader, readFile(vertexShader));
        glCompileShader(vShader);
        if (glGetShaderi(vShader, GL_COMPILE_STATUS) != 1)
            System.err.println(glGetShaderInfoLog(vShader));
        fShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fShader, readFile(fragmentShader));
        glCompileShader(fShader);
        if (glGetShaderi(fShader, GL_COMPILE_STATUS) != 1)
            System.err.println(glGetShaderInfoLog(fShader));
        program = glCreateProgram();
        glAttachShader(program, vShader);
        glAttachShader(program, fShader);
        glBindAttribLocation(program, 0, "vertices");
        glLinkProgram(program);
        if (glGetProgrami(program, GL_LINK_STATUS) != 1) {
            System.err.println(glGetProgramInfoLog(program));
            System.exit(1);
        }
    }

    public void bind() {
        glUseProgram(program);
    }

    public void unbind() {
        glUseProgram(0);
    }

    public void setTransformationMatrixObject(float translationX, float translationY, float depth, float rotation,
            float scaleX, float scaleY) {
        Matrix4f matrix = new Matrix4f();
        matrix.translate(translationX, translationY, depth);
        matrix.rotate((float) Math.toRadians(rotation), 0, 0, 1);
        matrix.scale(scaleX, scaleY, 1);
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        matrix.get(buffer);
        int location = glGetUniformLocation(program, "transformationMatrixObject");
        if (location != -1) {
            glUniformMatrix4fv(location, false, buffer);
        }
    }

    public void setTransformationMatrixCamera(float x, float y, float rotation, float scale) {
        Matrix4f matrix = new Matrix4f();
        matrix.scale(scale, scale, 1);
        matrix.rotate((float) Math.toRadians(rotation), 0, 0, 1);
        matrix.translate(x, y, 0);
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        matrix.get(buffer);
        int location = glGetUniformLocation(program, "transformationMatrixCamera");
        if (location != -1) {
            glUniformMatrix4fv(location, false, buffer);
        }
    }

    public void setOrtographicProjectionMatrix(float width, float height, float near, float far) {
        Matrix4f matrix = new Matrix4f().ortho(-width / 2, width / 2, -height / 2, height / 2, near, far);
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        matrix.get(buffer);
        
        int location = glGetUniformLocation(program, "orthoProjectionMatrix");
        if (location != -1) {
            glUniformMatrix4fv(location, false, buffer);
        }

    }

    public void setColorToAdd(Color colorToAdd) {
        int location = glGetUniformLocation(program, "colorToAdd");
        if (location != -1) {
            glUniform4fv(location, colorToAdd.getRGBA());
        }
    }
}
