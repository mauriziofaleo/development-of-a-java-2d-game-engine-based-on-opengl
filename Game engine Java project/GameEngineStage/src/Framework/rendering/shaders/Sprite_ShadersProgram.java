package Framework.rendering.shaders;

public class Sprite_ShadersProgram extends ShadersProgram {
    public static Sprite_ShadersProgram shaderInstance;

    public Sprite_ShadersProgram() {
        createProgram("Sprite_Shader.vs", "Sprite_Shader.fs");
        shaderInstance = this;
    }
}
