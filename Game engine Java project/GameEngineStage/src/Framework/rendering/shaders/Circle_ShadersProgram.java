package Framework.rendering.shaders;

import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glUniform4fv;

import Framework.Color;

public class Circle_ShadersProgram extends ShadersProgram {
    public static Circle_ShadersProgram shaderInstance;

    public Circle_ShadersProgram() {
        this.createProgram("Circle_Shader.vs", "Circle_Shader.fs");
        shaderInstance = this;
    }

    public void setInternalColor(Color color) {
        int location = glGetUniformLocation(program, "internalColor");
        if (location != -1) {
            glUniform4fv(location, color.getRGBA());
        }
    }

    public void setExternalColor(Color color) {
        int location = glGetUniformLocation(program, "externalColor");
        if (location != -1) {
            glUniform4fv(location, color.getRGBA());
        }
    }
}
