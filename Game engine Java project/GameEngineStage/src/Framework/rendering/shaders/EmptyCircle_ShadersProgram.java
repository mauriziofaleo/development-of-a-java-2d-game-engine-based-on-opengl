package Framework.rendering.shaders;

import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glUniform4fv;

import Framework.Color;

public class EmptyCircle_ShadersProgram extends ShadersProgram {
    public static EmptyCircle_ShadersProgram shaderInstance;

    public EmptyCircle_ShadersProgram() {
        createProgram("EmptyCircle_Shader.vs", "EmptyCircle_Shader.fs");
        shaderInstance = this;
    }

    public void setColor(Color color) {
        int location = glGetUniformLocation(program, "color");
        if (location != -1) {
            glUniform4fv(location, color.getRGBA());
        }
    }
}
