package Framework.rendering.actors;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL45.glNamedBufferStorage;
import Framework.Color;
import Framework.MemoryManager;
import Framework.rendering.interfaces.Renderable;
import Framework.rendering.shaders.Circle_ShadersProgram;

public abstract class CircleActor extends Renderable {
    // LOCAL ATTRIBUTES
    private float radius;
    private Color internalColor;
    private Color externalColor;
    // STATIC CODE
    static int vao;
    static int triangles = 360;
    static int BufferVertex;
    static {
        float[] vertexCoord = new float[triangles * 3 + 3 + 3];
        for (int n = 0; n < 3; n++) {
            vertexCoord[n] = 0; // CENTRAL VERTEX IN POSITION (0,0,0)
        }
        for (int n = 0; n <= triangles; n++) {
            vertexCoord[3 + n * 3] = (float) (Math.cos((float) ((Math.PI * 2 * n) / triangles)));     // x
            vertexCoord[3 + n * 3 + 1] = (float) (Math.sin((float) ((Math.PI * 2 * n) / triangles))); // y
            vertexCoord[3 + n * 3 + 2] = 0;                                                           // z
        }
        BufferVertex = MemoryManager.getNewVBO();
        glNamedBufferStorage(BufferVertex, vertexCoord, 0);
        vao = MemoryManager.getNewVAO();
        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, BufferVertex);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
        glDisableVertexAttribArray(0);
        glBindVertexArray(0);
    }

    // CONSTRUCTOR
    public CircleActor(int radius, Color internalColor, Color externalColor) {
        this.radius = radius;
        this.internalColor = internalColor;
        this.externalColor = externalColor;
    }

    // METHODS
    public void render() {
        if (visible) {
            Circle_ShadersProgram.shaderInstance.bind();
            glBindVertexArray(vao);
            glEnableVertexAttribArray(0);
            Circle_ShadersProgram.shaderInstance.setTransformationMatrixObject(
                    x, y, -distance, rotation, radius * scaleX, radius * scaleY);
            Circle_ShadersProgram.shaderInstance.setColorToAdd(colorToAdd);
            Circle_ShadersProgram.shaderInstance.setInternalColor(internalColor);
            Circle_ShadersProgram.shaderInstance.setExternalColor(externalColor);
            glDrawArrays(GL_TRIANGLE_FAN, 0, triangles + 2);
            glDisableVertexAttribArray(0);
            glBindVertexArray(0);
            Circle_ShadersProgram.shaderInstance.unbind();
        }
    }

    public void setColorToAdd(Color colorToAdd) {
        this.colorToAdd = colorToAdd;
    }

    public Color getColorToAdd() {
        return colorToAdd;
    }

    public void setRaggio(float radius) {
        this.radius = radius;
    }

    public float getRadius() {
        return radius;
    }
   
}
