package Framework.rendering.actors;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL45.*;
import Framework.Color;
import Framework.MemoryManager;
import Framework.rendering.interfaces.Renderable;
import Framework.rendering.shaders.RectangleLine_ShadersProgram;

public abstract class EmptyRectangleActor extends Renderable {
    // LOCAL ATTRIBUTES
    private int vao;
    private int BufferColor;
    private int thickness = 1;
    private int width;
    private int height;
    // STATIC CODE
    static int BufferVertex=MemoryManager.getNewVBO();
    static {
        float[] vertexCoord = new float[] { -1f / 2, +1f / 2, 0, // TOP LEFT
                -1f / 2, -1f / 2, 0, // BOTTOM LEFT
                +1f / 2, -1f / 2, 0, // BOTTOM RIGHT
                +1f / 2, +1f / 2, 0 // TOP RIGHT
        };
        glNamedBufferStorage(BufferVertex, vertexCoord, 0);
    }

    // CONSTRUCTORS
    public EmptyRectangleActor(int width, int height, Color colorTopLeft, Color colorBottomLeft, Color colorBottomRight,
            Color colorTopRight) {
        this.width = width;
        this.height = height;
        vao = MemoryManager.getNewVAO();
        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, BufferVertex);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
        BufferColor = MemoryManager.getNewVBO();
        float[] color = new float[16];
        for (int n = 0; n < 4; n++)
            color[n] = colorTopLeft.getRGBA()[n];
        for (int n = 4; n < 8; n++)
            color[n] = colorBottomLeft.getRGBA()[n - 4];
        for (int n = 8; n < 12; n++)
            color[n] = colorBottomRight.getRGBA()[n - 8];
        for (int n = 12; n < 16; n++)
            color[n] = colorTopRight.getRGBA()[n - 12];
        glNamedBufferStorage(BufferColor, color, 0);
        glBindBuffer(GL_ARRAY_BUFFER, BufferColor);
        glVertexAttribPointer(1, 4, GL_FLOAT, false, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glBindVertexArray(0);
    }

    // METHODS
    public void render() {
        if (visible) {
            glLineWidth(thickness);
            RectangleLine_ShadersProgram.shaderInstance.bind();
            glBindVertexArray(vao);
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            RectangleLine_ShadersProgram.shaderInstance.setTransformationMatrixObject(x, y, -distance, rotation,
                    scaleX * width, scaleY * height);
            RectangleLine_ShadersProgram.shaderInstance.setColorToAdd(colorToAdd);
            glDrawArrays(GL_LINE_LOOP, 0, 4);
            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
            glBindVertexArray(0);
            RectangleLine_ShadersProgram.shaderInstance.unbind();
        }
    }

    public void setThickness(int thickness) {
        this.thickness = thickness;
    }

    public int getThickness() {
        return thickness;
    }

    public void setWidth(float width) {
        this.width = (int) width;
    }

    public void setHeight(float height) {
        this.height = (int) height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
