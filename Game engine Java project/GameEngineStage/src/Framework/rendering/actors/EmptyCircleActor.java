package Framework.rendering.actors;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL45.glNamedBufferStorage;
import Framework.Color;
import Framework.MemoryManager;
import Framework.rendering.interfaces.Renderable;
import Framework.rendering.shaders.EmptyCircle_ShadersProgram;

public abstract class EmptyCircleActor extends Renderable {
    // LOCAL ATTRIBUTES
    private int thickness = 1;
    private float radius;
    private Color color;
    // STATIC CODE
    static int vao;
    static int segments = 360;
    static int BufferVertex=MemoryManager.getNewVBO();
    static {
        float[] vertexCoord = new float[segments * 3];
        for (int n = 0; n < segments; n++) {
            vertexCoord[n * 3] = (float) (Math.cos((float) ((Math.PI * 2 * n) / segments)));     // x
            vertexCoord[n * 3 + 1] = (float) (Math.sin((float) ((Math.PI * 2 * n) / segments))); // y
            vertexCoord[n * 3 + 2] = 0;                                                          // z
        }
        glNamedBufferStorage(BufferVertex, vertexCoord, 0);
        vao = MemoryManager.getNewVAO();
        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, BufferVertex);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDisableVertexAttribArray(0);
        glBindVertexArray(0);
    }

    // CONSTRUCTOR
    public EmptyCircleActor(int radius, Color color) {
        this.radius = radius;
        this.color = color;
    }

    // METHODS
    public void render() {
        if (visible) {
            glLineWidth(thickness);
            EmptyCircle_ShadersProgram.shaderInstance.bind();
            glBindVertexArray(vao);
            glEnableVertexAttribArray(0);
            EmptyCircle_ShadersProgram.shaderInstance.setTransformationMatrixObject(
                    x, y, -distance, rotation,radius * scaleX, radius * scaleY);
            EmptyCircle_ShadersProgram.shaderInstance.setColorToAdd(colorToAdd);
            EmptyCircle_ShadersProgram.shaderInstance.setColor(color);
            glDrawArrays(GL_LINE_LOOP, 0, segments);
            glDisableVertexAttribArray(0);
            glBindVertexArray(0);
            EmptyCircle_ShadersProgram.shaderInstance.unbind();
        }
    }

    public void setColorToAdd(Color colorToAdd) {
        this.colorToAdd = colorToAdd;
    }

    public Color getColorToAdd() {
        return colorToAdd;
    }

    public void setThickness(int thickness) {
        this.thickness = thickness;
    }

    public int getThickness() {
        return thickness;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getRadius() {
        return radius;
    }
    
}
