package Framework.rendering.actors;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL45.glNamedBufferStorage;
import Framework.Color;
import Framework.MemoryManager;
import Framework.rendering.interfaces.Renderable;
import Framework.rendering.shaders.RectangleLine_ShadersProgram;

public abstract class LineActor extends Renderable {
    // LOCAL ATTRIBUTES
    private int vao;
    private int BufferColor;
    private int length;
    private int thickness = 1;
    // STATIC CODE
    static int BufferVertex;
    static {
        float[] vertexCoord = new float[] { 
                -1f / 2, 0, 0, // STARTING VERTEX
                +1f / 2, 0, 0, // FINISH VERTEX
        };
        BufferVertex = MemoryManager.getNewVBO();
        glNamedBufferStorage(BufferVertex, vertexCoord, 0);
    }

    // CONSTRUCTOR
    public LineActor(int length, Color startingColor, Color endingColor) {
            this.length = length;
            vao = MemoryManager.getNewVAO();
            glBindVertexArray(vao);
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            glBindBuffer(GL_ARRAY_BUFFER, BufferVertex);
            glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
            BufferColor = MemoryManager.getNewVBO();
            float[] color = new float[8];
            for (int n = 0; n < 4; n++)
                color[n] = startingColor.getRGBA()[n];
            for (int n = 4; n < 8; n++)
                color[n] = endingColor.getRGBA()[n - 4];
            glNamedBufferStorage(BufferColor, color, 0);
            glBindBuffer(GL_ARRAY_BUFFER, BufferColor);
            glVertexAttribPointer(1, 4, GL_FLOAT, false, 0, 0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
            glBindVertexArray(0);
    }

    // METHODS
    public void render() {
        if (visible) {
            glLineWidth(thickness);
            RectangleLine_ShadersProgram.shaderInstance.bind();
            glBindVertexArray(vao);
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            RectangleLine_ShadersProgram.shaderInstance.setTransformationMatrixObject(x, y, -distance, rotation,
                    scaleX * length, scaleY * 1);
            RectangleLine_ShadersProgram.shaderInstance.setColorToAdd(colorToAdd);
            glDrawArrays(GL_LINES, 0, 2);
            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
            glBindVertexArray(0);
            RectangleLine_ShadersProgram.shaderInstance.unbind();
        }
    }

    public void setThickness(int thickness) {
        this.thickness = thickness;
    }

    public int getThickness() {
        return thickness;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }

}
