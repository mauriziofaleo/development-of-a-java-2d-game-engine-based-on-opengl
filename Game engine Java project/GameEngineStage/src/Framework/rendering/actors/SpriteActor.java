package Framework.rendering.actors;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL45.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import Framework.MemoryManager;
import Framework.rendering.interfaces.Renderable;
import Framework.rendering.shaders.Sprite_ShadersProgram;

public abstract class SpriteActor extends Renderable {
    // ATTRIBUTES
    private int BufferTexture;
    private int BufferVertex;
    private int vao;
    private String imageName;
    // STATIC
    static float[] textureCoord = new float[] { 
            0, 0, // UPPER LEFT
            0, 1, // BOTTOM LEFT
            1, 1, // BOTTOM RIGHT
            1, 0 }; // UPPER RIGHT
    static Integer BufferTexCoord = MemoryManager.getNewVBO();
    static {
        glNamedBufferStorage(BufferTexCoord, textureCoord, 0);
    }

    // CONSTRUCTOR
    public SpriteActor(String imageName) throws IOException {
            LoadImage(imageName);
            this.imageName = imageName;
    }

    private void LoadImage(String imageName) throws IOException {
        BufferedImage bi;
        bi = ImageIO.read(new File("res/" + imageName));
        int widthSprite = bi.getWidth();
        int heightSprite = bi.getHeight();
        ByteBuffer pixels = BufferUtils.createByteBuffer(widthSprite * heightSprite * 4);
        for (int y = 0; y < heightSprite; y++) {
            for (int x = 0; x < widthSprite; x++) {
                int value = bi.getRGB(x, y);
                pixels.put((byte) ((value & 0x00FF0000) >> 16));
                pixels.put((byte) ((value & 0x0000FF00) >> 8));
                pixels.put((byte) (value & 0x000000FF));
                pixels.put((byte) ((value & 0xFF000000) >> 24));
            }
        }
        pixels.flip();
        BufferTexture = MemoryManager.getNewTextureBuffer();
        glBindTexture(GL_TEXTURE_2D, BufferTexture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, widthSprite, heightSprite, 0, GL_RGBA, GL11.GL_UNSIGNED_BYTE, pixels);
        glBindTexture(GL_TEXTURE_2D, 0);
        float[] vertexCoord = new float[] { -widthSprite / 2, +heightSprite / 2, 0, // TOP LEFT
                -widthSprite / 2, -heightSprite / 2, 0, // BOTTOM LEFT
                +widthSprite / 2, -heightSprite / 2, 0, // BOTTOM RIGHT
                +widthSprite / 2, +heightSprite / 2, 0 // TOP RIGHT
        };
        BufferVertex = MemoryManager.getNewVBO();
        glNamedBufferStorage(BufferVertex, vertexCoord, 0);
        vao = MemoryManager.getNewVAO();
        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, BufferVertex);
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, BufferTexCoord);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glBindVertexArray(0);
    }

    public void render() {
        if (visible) {
            Sprite_ShadersProgram.shaderInstance.bind();
            glBindVertexArray(vao);
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            Sprite_ShadersProgram.shaderInstance.setTransformationMatrixObject(x, y, -distance, rotation, scaleX, scaleY);
            Sprite_ShadersProgram.shaderInstance.setColorToAdd(colorToAdd);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, BufferTexture);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glDrawArrays(GL_QUADS, 0, 4);
            glBindTexture(GL_TEXTURE_2D, 0);
            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
            glBindVertexArray(0);
            Sprite_ShadersProgram.shaderInstance.unbind();
        }
    }

    public void setImage(String imageName) throws IOException {
        LoadImage(imageName);
        this.imageName = imageName;
    }

    public String getImage() {
        return imageName;
    }
}
