package Framework;

import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL45.glCreateBuffers;
import static org.lwjgl.opengl.GL45.glCreateVertexArrays;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;

public class MemoryManager {
    static ArrayList<Integer> VAOs = new ArrayList<Integer>();
    static ArrayList<Integer> VBOs = new ArrayList<Integer>();
    static ArrayList<Integer> Textures = new ArrayList<Integer>();

    public static int getNewVBO() {
        int VBO = glCreateBuffers();
        VBOs.add(VBO);
        return VBO;
    }

    public static int getNewVAO() {
        int VAO = glCreateVertexArrays();
        VAOs.add(VAO);
        return VAO;
    }

    public static int getNewTextureBuffer() {
        int BufferTexture = glGenTextures();
        Textures.add(BufferTexture);
        return BufferTexture;
    }
    
    public static void freeMemory() {
        for (int i : VAOs) {
            GL30.glDeleteVertexArrays(i);
        }
        for (int i : VBOs) {
            GL15.glDeleteBuffers(i);
        }
        for (int i : Textures) {
            GL11.glDeleteTextures(i);
        }
    }
}
