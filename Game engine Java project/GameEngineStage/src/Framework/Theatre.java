package Framework;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;
import Framework.rendering.shaders.Circle_ShadersProgram;
import Framework.rendering.shaders.EmptyCircle_ShadersProgram;
import Framework.rendering.shaders.RectangleLine_ShadersProgram;
import Framework.rendering.shaders.Sprite_ShadersProgram;

public class Theatre {
    // ATTRIBUTES
    private Scene currentScene;
    private long monitor = 0;
    private int WindowWidth, WindowHeight;
    private String WindowTitle;
    private float frameRate = 60;
    private boolean limitFrameRate = true;
    private float speed = 1;

    // CONSTRUCTORS
    public Theatre(int WindowWidth, int WindowHeight, String WindowTitle) {
        glfwInit();
        this.WindowWidth = WindowWidth;
        this.WindowHeight = WindowHeight;
        this.WindowTitle = WindowTitle;
        createTheatre();
    }

    public Theatre(String WindowTitle) {
        glfwInit();
        this.WindowTitle = WindowTitle;
        monitor = glfwGetPrimaryMonitor();
        this.WindowWidth = glfwGetVideoMode(monitor).width();
        this.WindowHeight = glfwGetVideoMode(monitor).height();
        createTheatre();
    }

    // METHODS
    private void createTheatre() {
        long window = glfwCreateWindow(WindowWidth, WindowHeight, WindowTitle, monitor, 0);
        glfwMakeContextCurrent(window);
        glfwSetWindowSizeCallback(window, new GLFWWindowSizeCallback() {
            @Override
            public void invoke(long window, int width, int height) {
                float aspectRatioCamera = ((float) currentScene.camera.getWidth()) / currentScene.camera.getHeight();
                int newWidth, newHeight;
                if (aspectRatioCamera < (((float) width) / height)) {
                    newHeight = height;
                    newWidth = (int) (aspectRatioCamera * newHeight);
                    glViewport((width - newWidth) / 2, 0, newWidth, newHeight);
                } else {
                    newWidth = width;
                    newHeight = (int) (((float) newWidth) / aspectRatioCamera);
                    glViewport(0, (height - newHeight) / 2, newWidth, newHeight);
                }
            }
        });
        GL.createCapabilities();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LESS);
        new Sprite_ShadersProgram();
        new RectangleLine_ShadersProgram();
        new EmptyCircle_ShadersProgram();
        new Circle_ShadersProgram();
    }

    public void act() {
        if (currentScene == null) {
            throw new FrameworkException("No scene attached to the theatre object to act");
        } 
        else {
            double timeSinceLastActCall = Double.MAX_VALUE;
            while (!glfwWindowShouldClose(glfwGetCurrentContext())) {
                long startTime = System.nanoTime();
                draw();    
                if (timeSinceLastActCall == Double.MAX_VALUE)
                    currentScene.act(((float) 1f / frameRate) * speed);
                else {
                    float deltaTime = ((float) timeSinceLastActCall / 1000000000);
                    if (deltaTime > 0.1f)
                        deltaTime = 0.1f;
                    currentScene.act(deltaTime * speed);
                }
                if (limitFrameRate) {
                    while (System.nanoTime() - startTime < (1000000000 / frameRate)) {
                    }
                }
                timeSinceLastActCall = System.nanoTime() - startTime;
            }
            glfwDestroyWindow(glfwGetCurrentContext());
            glfwTerminate();
        }
    }

    private void draw() {
        glClearBufferfv(GL_COLOR, 0, currentScene.getBackgroundColor().getRGB());
        glClear(GL_DEPTH_BUFFER_BIT);
        currentScene.render();
        glfwSwapBuffers(glfwGetCurrentContext());
        glfwPollEvents();
    }

    public void limitFrameRate(boolean limitFrameRate) {
        this.limitFrameRate = limitFrameRate;
    }

    public void setFrameRate(int frameRate) {
        if (frameRate <= 10)
            throw new FrameworkException("Impossible to set a framerate <= 10FPS");
        this.frameRate = frameRate;
    }

    public void setScene(Scene sceneToSet) {
        if (sceneToSet.getCamera() == null) {
            throw new FrameworkException("No camera attached to the scene object to set");
        }
        currentScene = sceneToSet;
        currentScene.getCamera().updateOrtographicProjectionMatrix();
    }

    public Scene getScene() {
        return currentScene;
    }
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getSpeed() {
        return speed;
    }
}
